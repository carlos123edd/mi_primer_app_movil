import firebase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyCGPU_HZFBFOLwIkDJFwf1rFsrOi-JEwZM",
  authDomain: "ejemploproyecto-68488.firebaseapp.com",
  databaseURL: "https://ejemploproyecto-68488.firebaseio.com",
  projectId: "ejemploproyecto-68488",
  storageBucket: "ejemploproyecto-68488.appspot.com",
  messagingSenderId: "998721815967",
  appId: "1:998721815967:web:555a47d220b681cf5729aa",
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
