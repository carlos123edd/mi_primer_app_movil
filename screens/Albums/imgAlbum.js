import React, { useState, useEffect } from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";
import { Text, Image } from "react-native-elements";
export default function imgAlbum(props) {
  const [isLoading, setLoading] = useState(true);
  const [informacion, setInformacion] = useState([]);
  const [image, setImage] = useState([]);
  const { albumId } = props.route.params;

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/albums/" + albumId)
      .then((response) => response.json())
      .then((json) => setInformacion(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/photos/" + albumId)
      .then((response) => response.json())
      .then((json) => setImage(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  function DataMusic() {
    return (
      <View style={styles.container}>
        <Image
          source={{ uri: image.url }}
          style={{ width: 200, height: 200 }}
          PlaceholderContent={<ActivityIndicator />}
        />
        <Text style={styles.titulo}>{informacion.title}</Text>
      </View>
    );
  }
  return (
    <View style={{ flex: 1, padding: 24 }}>
      {isLoading ? <ActivityIndicator /> : <DataMusic />}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 3,
    alignItems: "center",
  },
  titulo: {
    marginBottom: 15,
    fontStyle: "italic",
  },
});
