import React, { useEffect, useState } from "react";
import { ActivityIndicator, FlatList, ScrollView } from "react-native";
import { ListItem } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";

export default function Lista() {
  const navigation = useNavigation();
  const [isLoading, setLoading] = useState(true);
  const [objeto, setData] = useState([]);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/albums")
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  function renderItem({ item }) {
    return (
      <ListItem
        title={item.title}
        leftAvatar={{ icon: { name: "folder", type: "font-awesome" } }}
        bottomDivider
        chevron
        onPress={() => navigation.navigate("imgAlbum", { albumId: item.id })}
      />
    );
  }

  return (
    <ScrollView style={{ flex: 1, padding: 24 }}>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          data={objeto}
          keyExtractor={({ id }, index) => id}
          renderItem={renderItem}
        />
      )}
    </ScrollView>
  );
}
