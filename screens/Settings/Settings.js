import React from "react";
import { View, Text, Button } from "react-native";
import { useNavigation } from "@react-navigation/native";

export default function Settings() {
  const navigation = useNavigation();
  return (
    <View>
      <Text>Pantalla de Settings</Text>
      <Button
        title="Hola"
        onPress={() => navigation.navigate("password")}
      ></Button>
    </View>
  );
}
