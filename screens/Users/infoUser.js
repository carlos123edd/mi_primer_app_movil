import React from "react";
import { View, StyleSheet } from "react-native";
import { Text, Avatar } from "react-native-elements";

export default function infoUser(props) {
  const user = props["route"]["params"]["user"];
  console.log(user);

  function DataUsuario() {
    return (
      <View style={styles.container}>
        <Text h2>{user.name}</Text>
        <Avatar
          containerStyle={styles.styleIcon}
          rounded
          icon={{ name: "user", type: "font-awesome" }}
          size="large"
        />
        <Text style={styles.subtitle}>{user.username}</Text>
        <Text style={styles.subtitle}>{user.email}</Text>
        <Text style={styles.title}>Dirección</Text>
        <Text style={styles.body}>
          Calle: {user.address.street} #{user.address.suite}
        </Text>
        <Text style={styles.body}>Ciudad: {user.address.city}</Text>
        <Text style={styles.body}>C.P. {user.address.zipcode}</Text>
        <Text style={styles.title}>Trabajo</Text>
        <Text style={styles.body}>{user.company.name}</Text>
        <Text style={styles.body}>{user.company.catchPhrase}</Text>
      </View>
    );
  }

  return (
    <View style={{ flex: 1, padding: 24 }}>
      <DataUsuario />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  styleIcon: {
    marginBottom: 12,
    marginTop: 12,
  },
  subtitle: {
    fontSize: 24,
  },
  title: {
    fontSize: 21,
    fontStyle: "italic",
    marginTop: 20,
    marginBottom: 10,
  },
  body: {
    fontSize: 15,
    marginTop: 5,
  },
});
