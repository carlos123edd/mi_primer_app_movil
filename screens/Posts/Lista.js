import React, { useEffect, useState } from "react";
import { ActivityIndicator, FlatList, Text, ScrollView } from "react-native";
import { Card, Button } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";

export default function Lista() {
  const navigation = useNavigation();
  const [isLoading, setLoading] = useState(true);
  const [objeto, setData] = useState([]);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  function renderItem({ item }) {
    return (
      <Card title={item.title}>
        <Text style={{ marginBottom: 10 }}>{item.body}</Text>
        <Button
          title="Ver comentarios"
          onPress={() =>
            navigation.navigate("comentariosPost", { postId: item.id })
          }
        ></Button>
      </Card>
    );
  }

  return (
    <ScrollView style={{ flex: 1, padding: 24 }}>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          data={objeto}
          keyExtractor={({ id }) => id}
          renderItem={renderItem}
        />
      )}
    </ScrollView>
  );
}
