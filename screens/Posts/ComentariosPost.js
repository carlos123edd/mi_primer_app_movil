import React, { useEffect, useState } from "react";
import { ActivityIndicator, FlatList, Text, ScrollView } from "react-native";
import { Card, Badge } from "react-native-elements";

export default function ComentariosPost(props) {
  const { postId } = props.route.params;
  const [isLoading, setLoading] = useState(true);
  const [comments, setComments] = useState([]);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts/" + postId + "/comments")
      .then((response) => response.json())
      .then((json) => setComments(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  function renderItem({ item }) {
    return (
      <Card title={item.name}>
        <Text style={{ marginBottom: 10 }}>{item.body}</Text>
        <Badge value={item.email} status="success" />
      </Card>
    );
  }

  return (
    <ScrollView style={{ flex: 1 }}>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          data={comments}
          keyExtractor={({ id }) => id}
          renderItem={renderItem}
        />
      )}
    </ScrollView>
  );
}
