import React, { useEffect } from "react";
import { StyleSheet, View } from "react-native";
//import Componente from "./components/Componente";
//import Informacion from "./components/Informacion";
//import Pantalla from "./components/Pantalla";
import Navegation from "./navegations/Navegation";
import { firebaseApp } from "./config/Firebase";
import * as firebase from "firebase";
import Componente from "./components/Componente";

export default function App() {
  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      console.log("El usuario es: " + user);
    });
  }, []);
  return (
    <View style={styles.container}>
      <Componente />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
