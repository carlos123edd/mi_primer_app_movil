import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Posts from "../screens/Posts/Lista";
import ComentariosPost from "../screens/Posts/ComentariosPost";

const stack = createStackNavigator();
export default function PostsStack() {
  return (
    <stack.Navigator>
      <stack.Screen name="post" component={Posts} options={{ title: "Post" }} />

      <stack.Screen
        name="comentariosPost"
        component={ComentariosPost}
        options={{ title: "Comentarios" }}
      />
    </stack.Navigator>
  );
}
