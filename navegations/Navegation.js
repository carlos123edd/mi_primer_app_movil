import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Icon } from "react-native-elements";

import UsersStack from "../navegations/UsersStack";
import PostsStack from "../navegations/PostsStack";
import AlbumsStack from "../navegations/AlbumsStack";
import Login from "../components/Login";

const Tab = createBottomTabNavigator();

export default function Navegation() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === "users") {
              iconName = focused ? "user" : "user";
            } else if (route.name === "posts") {
              iconName = focused ? "envelope" : "envelope";
            } else if (route.name === "albums") {
              iconName = focused ? "folder" : "folder-o";
            }
            return (
              <Icon
                type="font-awesome"
                name={iconName}
                color={color}
                size={size}
              />
            );
          },
        })}
        tabBarOptions={{
          activeTintColor: "blue",
          inactiveTintColor: "black",
        }}
      >
        <Tab.Screen
          name="users"
          component={UsersStack}
          options={{ title: "Usuarios" }}
        ></Tab.Screen>
        <Tab.Screen
          name="posts"
          component={PostsStack}
          options={{ title: "Posts" }}
        />
        <Tab.Screen
          name="albums"
          component={AlbumsStack}
          options={{ title: "Albums" }}
        />
        <Tab.Screen
          name="Login"
          component={Login}
          options={{ title: "Login" }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
