import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Albums from "../screens/Albums/Lista";
import imgAlbum from "../screens/Albums/imgAlbum";

const stack = createStackNavigator();
export default function AlbumsStack() {
  return (
    <stack.Navigator>
      <stack.Screen
        name="albums"
        component={Albums}
        options={{ title: "Albums" }}
      />
      <stack.Screen
        name="imgAlbum"
        component={imgAlbum}
        options={{ title: "Contenido del album" }}
      />
    </stack.Navigator>
  );
}
