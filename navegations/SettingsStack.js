import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Settings from "../screens/Settings/Settings";
import Password from "../screens/Settings/Password";

const stack = createStackNavigator();
export default function SettingsStack() {
  return (
    <stack.Navigator>
      <stack.Screen
        name="settings"
        component={Settings}
        options={{ title: "Configuraciones" }}
      />
      <stack.Screen
        name="password"
        component={Password}
        options={{ title: "Cambiar password" }}
      />
    </stack.Navigator>
  );
}
