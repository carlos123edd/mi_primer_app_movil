import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Lista from "../screens/Users/Lista";
import infoUser from "../screens/Users/infoUser";

const stack = createStackNavigator();
export default function UsersStack() {
  return (
    <stack.Navigator>
      <stack.Screen
        name="lista"
        component={Lista}
        options={{ title: "Usuarios" }}
      />
      <stack.Screen
        name="infoUser"
        component={infoUser}
        options={{ title: "Detalles" }}
      />
    </stack.Navigator>
  );
}
