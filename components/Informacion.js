import React, { useState, useEffect } from "react";
import { View, Text, Button } from "react-native";

export default function Informacion(props) {
  const [final, setEstado] = useState(true);
  useEffect(() => {
    console.log("cargando mas elementos");
  }, [final]);
  function cambiar() {
    setEstado(!final);
  }
  console.log(props);
  const { info, saludar } = props;
  const {
    nombre = "Nombre indefinido",
    apellido = "Apellido indefinido",
    edad = "Edad indefinida",
  } = info;
  return (
    <View>
      <Text>
        Hola {nombre} {apellido} y la edad es {edad} y esta{" "}
        {final ? "vivo" : "muerto"}
      </Text>
      <Button title="Saludando" onPress={() => cambiar()}></Button>
    </View>
  );
}
