import React, { useState } from "react";
import { StyleSheet, Text, View, Button, TextInput } from "react-native";
import * as firebase from "firebase";

export default function Login() {
  const [Nombre, setNombre] = useState("");
  const [Password, setPassword] = useState("");
  function preesionar() {
    //console.log(Nombre + "_" + Password);
    firebase
      .auth()
      .createUserWithEmailAndPassword(Nombre, Password)
      .then((response) => {
        console.log(response);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  return (
    <View>
      <TextInput
        placeholder="Ingresar nombre"
        style={styles.input}
        onChange={(e) => {
          setNombre(e.nativeEvent.text);
        }}
      ></TextInput>
      <TextInput
        placeholder="Ingresar contraseña"
        style={styles.input}
        onChange={(e) => {
          setPassword(e.nativeEvent.text);
        }}
      ></TextInput>
      <Button title="Acepto" color="gray" onPress={preesionar}></Button>
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    width: "100%",
    height: 40,
    borderColor: "yellow",
    borderWidth: 2,
  },
});
