import React, { useState } from "react";
import { View, StyleSheel, Text } from "react-native";
import { Header, Input, Button } from "react-native-elements";

export default function Pantalla() {
  const [nombre, setNombre] = useState("");
  const separador = "";
  const sep = nombre.split(separador);
  for (var i = 0; i < sep.length; i++) {
    sep[i] = "?" + sep[i] + "?";
  }
  console.log("" + sep + "");
  function presionar() {
    setNombre("" + sep + "");
  }
  return (
    <View>
      <Header
        leftComponent={{ icon: "menu", color: "#fff" }}
        centerComponent={{ text: "Practica 1", style: { color: "#fff" } }}
        containerStyle={{
          backgroundColor: "red",
          justifyContent: "space-around",
        }}
        rightComponent={{ icon: "home", color: "#fff" }}
      />
      <Input
        placeholder="Ingresar nombre"
        onChange={(e) => {
          setNombre(e.nativeEvent.text);
        }}
      />
      <Button title="Aceptar" color="red" onPress={() => presionar()} />
      <Text>Hola que hace {nombre}</Text>
    </View>
  );
}
