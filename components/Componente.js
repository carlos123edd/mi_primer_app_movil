import React, { useState } from "react";
import { StyleSheet, Text, View, Button, TextInput } from "react-native";
import * as firebase from "firebase";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
//import { firebaseApp } from "../config/Firebase";

export default function Componente() {
  const [Nombre, setNombre] = useState("");
  console.log(Nombre);
  function preesionar() {
    console.log("Hola que hace" + Nombre);
  }

  const subirImagen = async (uri) => {
    const response = await fetch(uri);
    const blob = await response.blob();
    const pet = firebase.storage().ref().child("imagenes/imagen3.jpg");
    pet.put(blob);
  };
  const SeleccionarImagen = async () => {
    const resultPermissions = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );
    const resultPermissionsCamera =
      resultPermissions.permissions.cameraRoll.status;

    if (resultPermissionsCamera === "demied") {
      alert("No tienes los permisos");
    } else {
      const result = await ImagePicker.launchCameraAsync({
        allowsEditing: true,
        aspect: [4, 3],
      });

      console.log(result);
      subirImagen(result.uri);
    }
  };

  return (
    <View>
      <TextInput
        placeholder="Ingresar nombre"
        style={styles.input}
        onChange={(e) => {
          setNombre(e.nativeEvent.text);
        }}
      ></TextInput>
      <Button title="Acepto" color="gray" onPress={SeleccionarImagen}></Button>
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    width: "100%",
    height: 40,
    borderColor: "yellow",
    borderWidth: 2,
  },
});
